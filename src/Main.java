import java.util.Calendar;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // call method that you want to test here
    }

    public static void firstTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter your name:");
        String name = scanner.next();

        System.out.println("Please enter your age:");
        int age = scanner.nextInt();

        System.out.println("Please enter your phone number:");
        String number = scanner.next();

        System.out.println("You data: " + name + ", " + age + ", " + number);

    }

    public static void secondTask() {

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        System.out.println("Enter your year of birth, please: ");

        int birthdayYear = new Scanner(System.in).nextInt();

        int age = currentYear - birthdayYear;

        System.out.println("Age: " + age);
    }

    public static void secondTask_Improved() {

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);

        System.out.println("Enter your year of birth, please: ");
        int birthdayYear = new Scanner(System.in).nextInt();

        System.out.println("Enter your moth of birth, please: ");
        int birthdayMonth = new Scanner(System.in).nextInt();

        int age = currentYear - birthdayYear;

        if (birthdayMonth > currentMonth) {
            age--;
        }

        System.out.println("Age: " + age);
    }


    public static void thirdTask_forLoop() {
        for (int i = 1; i <= 20; i++) {
            if (i % 2 == 0) {
                System.out.println("Even numbers are: " + i);
            }
        }
    }

    public static void thirdTask_whileLoop() {
        int k = 1;
        while (k <= 20) {
            if (k % 2 == 0) {
                System.out.println("Even numbers are: " + k);
            }

            k++;
        }
    }


    public static void thirdTask_doWhileLoop() {
        int k = 1;
        do {
            if (k % 2 == 0) {
                System.out.println("Even numbers are: " + k);
            }

            k++;

        } while (k <= 20);
    }

    private static void fourthTask() {
        int result = 0;
        int temp;

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        try {
            if (b > a) {
                temp = b;
                b = a;
                a = temp;
            }

            result = a / b;

        } catch (ArithmeticException e) {
            System.out.println("You should not divide a number by zero");
        }

        System.out.println("Result is " + result);

    }

    public static void fifthTask_IfElse() {

        Scanner scanner = new Scanner(System.in);

        int day = scanner.nextInt();

        if (day == 1) {
            System.out.println("Monday");
        } else if (day == 2) {
            System.out.println("Tuesday");
        } else if (day == 3) {
            System.out.println("Wednesday");
        } else if (day == 4) {
            System.out.println("Thursday");
        } else if (day == 5) {
            System.out.println("Friday");
        } else if (day == 6) {
            System.out.println("Saturday");
        } else {
            System.out.println("Sunday");
        }
    }

    public static void fifthTask() {

        Scanner scanner = new Scanner(System.in);

        int day = scanner.nextInt();

        switch (day) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
        }
    }
}
